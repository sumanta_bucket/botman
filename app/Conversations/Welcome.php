<?php

namespace App\Conversations;

use BotMan\BotMan\Messages\Conversations\Conversation;
use BotMan\BotMan\Messages\Incoming\Answer;
use BotMan\BotMan\Messages\Outgoing\Actions\Button;
use BotMan\BotMan\Messages\Outgoing\Question;

class Welcome extends Conversation
{

    protected $firstname;

    protected $email;

    protected $answer;

    /**
     *
     */
    public function getListExchanges()
    {
//        $this->ask('I can definitely help you in trading. Please select the exchanges that you are using.!', function (Answer $answer) {
            $question = Question::create('We support this exchanges')
                ->fallback('Unable to create a new database')
                ->callbackId('create_database')
                ->addButtons([
                    Button::create('kraken')->value('kraken'),
                    Button::create('binance')->value('binance'),
                ]);

            $this->ask($question, function (Answer $answer) {
                // Detect if button was clicked:
                if ($answer->isInteractiveMessageReply()) {
                    $selectedValue = $answer->getValue(); // will be either 'yes' or 'no'
                    $selectedText = $answer->getText(); // will be either 'Of course' or 'Hell no!'
                    $this->fetchExchnageData($selectedValue);
                }
            });
    }

    public function fetchExchnageData($data)
    {
        $this->say("You selected ");
    }

    public function askEmail()
    {
        $this->ask('One more thing - what is your email?', function (Answer $answer) {
            // Save result
            $this->email = $answer->getText();

            $this->say('Great - that is all we need, ' . $this->firstname);
            $this->willToGetTradeInfo();
        });
    }

    public function willToGetTradeInfo()
    {
        $this->ask('Shall we proceed? Say YES or NO', [
            [
                'pattern' => 'yes|yep',
                'callback' => function () {
                    $this->say('Okay - we\'ll keep going');
                    $this->answer = 'Okay - we\'ll keep going';
                }
            ],
            [
                'pattern' => 'nah|no|nope',
                'callback' => function () {
                    $this->say('PANIC!! Stop the engines NOW!');
                }
            ]
        ]);


    }

    public function startTradingSequence()
    {


//        $question = Question::create('Do you need a database?')
//            ->fallback('Unable to create a new database')
//            ->callbackId('create_database')
//            ->addButtons([
//                Button::create('Of course')->value('Of course'),
//                Button::create('Hell no!')->value('no'),
//            ]);
//
//        $this->ask($question, function (Answer $answer) {
//            // Detect if button was clicked:
//            if ($answer->isInteractiveMessageReply()) {
//                $selectedValue = $answer->getValue(); // will be either 'yes' or 'no'
//                $selectedText = $answer->getText(); // will be either 'Of course' or 'Hell no!'
//             \Log::info($selectedValue);
//             \Log::info($selectedText);
//
//            }
//        });

    }
//$botman->startConversation(new PizzaConversation(), 'my-recipient-user-id', TelegramDriver::class);

    /**
     * Start the conversation.
     *
     * @return mixed
     */
    public function run()
    {
        $this->getListExchanges();
    }
}
