<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Exchange extends Model
{
    use SoftDeletes;
    protected $guarded = ['id'];

    public function scopeActive($query)
    {
        return $query->where('is_active', config('exchange.status.active'));
    }
}
