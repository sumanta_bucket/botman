<?php

use App\Conversations\Welcome;
use App\Http\Controllers\BotManController;
use BotMan\Drivers\Facebook\Commands\AddStartButtonPayload;
use BotMan\Drivers\Facebook\Commands\AddGreetingText;

use BotMan\Drivers\Facebook\Extensions\ButtonTemplate;
use BotMan\Drivers\Facebook\Extensions\ElementButton;
$botman = resolve('botman');

//$botman->hears('Hi', function ($bot) {
//    $bot->reply('Hello!');
//});
//$botman->hears('what is your name?',function ($bot){
//   $bot->reply
//});
$botman->hears('Start conversation', BotManController::class . '@startConversation');
//$grettingtext = array("HI", "HELLO", "HEY", "GET", "START", "GET");

$botman->hears("hello", function($bot) {
    $bot->startConversation(new Welcome());
//    $bot->getTex
});
$botman->fallback(function($bot) {
    $bot->reply('Sorry, I did not understand these commands. Here is a list of commands I understand: ...');
});

$botman->hears('hi', function ($bot) {
    $bot->reply(ButtonTemplate::create('Do you want to know more about BotMan?')
        ->addButton(ElementButton::create('Help me in trading')
            ->type('postback')
            ->payload('tellmemore')
        )
        ->addButton(ElementButton::create('Crypto trading facts')
            ->type('postback')
            ->payload('tradingFacts')
        )
        ->addButton(ElementButton::create('My Subscriptions')
            ->type('postback')
            ->payload('mysubscriptions')
        )
    );
});

$botman->hears('tellmemore',function ($bot){
    $bot->startConversation(new Welcome());
});